# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Wizard, StateTransition, StateReport
from trytond.transaction import Transaction
from trytond.modules.company import CompanyReport

__all__ = ['PrintInvoice', 'Invoice']


class PrintInvoice(Wizard):
    """Print Invoice"""
    __name__ = 'account.print.invoice'

    start = StateTransition()
    print_ = StateReport('account.invoice')

    def transition_start(self):
        Invoice = Pool().get('account.invoice')
        invoices = Invoice.browse(Transaction().context['active_ids'])
        self.parties = self._get_parties(invoices)
        if self.parties:
            return 'print_'
        return 'end'

    def do_print_(self, action):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')
        Invoice = pool.get('account.invoice')
        ActionReport = pool.get('ir.action.report')
        Action = pool.get('ir.action')

        data = {}
        invoices = Invoice.browse(Transaction().context['active_ids'])

        self.current_party = self.parties[0]
        invoices = self._get_party_invoices(invoices, self.current_party)
        if invoices:
            data['ids'] = [i.id for i in invoices]
        self.parties = self.parties[1:]

        action_id = self._get_action_report()
        if action_id:
            action_report = ActionReport(Modeldata.get_id(*action_id))
            action = action_report.action
            action = Action.get_action_values(action.type, [action.id])[0]
        mechanism = self.current_party.contact_mechanism_get(
            'email', usage='invoice')
        if mechanism:
            action['email'] = {"to": mechanism.email}
        return action, data

    def transition_print_(self):
        if self.parties:
            return 'print_'
        return 'end'

    @classmethod
    def _get_parties(cls, invoices):
        return list(set(i.party for i in invoices))

    @classmethod
    def _get_party_invoices(self, invoices, party):
        return [invoice for invoice in invoices if invoice.party == party]

    @classmethod
    def _get_action_report(cls):
        pass


class Invoice(CompanyReport, metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    def _execute(cls, records, header, data, action):
        pool = Pool()
        Invoice = pool.get('account.invoice')

        invoices = Invoice.browse(records)
        report_context = cls.get_context(records, header, data)
        result = cls.convert(action, cls.render(action, report_context))

        if len(invoices) == 1:
            result = super()._execute(records, header, data, action)
        # clear cache
        for invoice in invoices:
            invoice.invoice_report_format = None
            invoice.invoice_report_cache = None
            invoice.save()
        return result
